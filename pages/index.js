import Head from "next/head";
import styled from "styled-components";
import {
  ImgHeader,
  HotelDetails,
  ButtonGroup,
  ChevronDirection
} from "../components/";
import { images } from "../public";
import { hotels } from "../data";
const StyledContainer = styled.div`
  .container {
    min-height: 100vh;
    padding: 0 0.5rem;
    display: flex;
    flex: 1;
    flex-direction: column;
    justify-content: space-between;
    align-items: leading;
  }
  header {
    justify-content: center;
    align-items: center;
    text-align: center;
  }
  footer {
    text-align: center;
    width: 100%;
    margin: 0 auto;
  }
`;

const HotelChicago = hotels[0];

const buttons = [
  {
    title: "Map",
    onClick: () => console.log("map"),
    chevron: ChevronDirection.Right
  },
  {
    title: "Photo Gallery",
    onClick: () => console.log("photo gallery"),
    chevron: ChevronDirection.Right
  },
  {
    title: "Amenities",
    onClick: () => console.log("amenities"),
    chevron: ChevronDirection.Right
  }
];
const Home = () => (
  <StyledContainer>
    <div className="container">
      <header>
        <ImgHeader uri={images.hotel} />
      </header>
      <main>
        <HotelDetails {...HotelChicago} />
      </main>

      <footer>
        <ButtonGroup buttons={buttons} />
      </footer>

      <style jsx>{`
        @media (max-width: 1000px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  </StyledContainer>
);

export default Home;
