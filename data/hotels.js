export const hotels = [
  {
    hotelName: "Hilton Chicago",
    address1: "720 South Michigan Avenue",
    address2: "Chicago, Illinois, 60605",
    phone: "1-312-922-4400"
  },
  {
    hotelName: "Hilton Boston",
    address1: "720 South Boston Avenue",
    address2: "Boston, Massachusetts, 60605",
    phone: "1-908-867-5309"
  }
];
