const images: { [index: string]: string } = {
  background: require("./images/background.png"),
  hotel: require("./images/hotelexterior.jpg"),
  logoSM: require("./images/hiltonbrandlogo.png"),
  logoLG: require("./images/HI_mk_logo_hiltonbrandlogo_3.jpg")
};

export default images;
