import React from "react";
import styled from "styled-components";
import { theme } from "../../public/";

export enum ChevronDirection {
  Left = "left",
  Right = "right"
}

export interface IButtonProps {
  title: string;
  onClick: () => void;
  chevron: ChevronDirection;
}

export const Button = (props: IButtonProps) => {
  const chevronClass = `chevron ${props.chevron}`;
  return (
    <button onClick={props.onClick}>
      {props.title}
      <span className={chevronClass}></span>
    </button>
  );
};

const StyledBackButton = styled.button`
  margin: 10px;
  border: none;
  display: flex;
  font-size: 1.2rem;
  color: #fff;
  width: 100px;
  height: 60px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  border-radius: 5px;
  text-align: center;
  padding: 15px;
  background-image: linear-gradient(${theme.darkGrey}, ${theme.black});
  .chevron::before {
    border-style: solid;
    border-width: 0.15em 0.15em 0 0;
    content: "";
    color: ${theme.white};
    display: inline-block;
    height: 0.6em;
    transform: rotate(-45deg);
    width: 0.6em;
  }

  .chevron.right:before {
    left: 0;
    right: 0;
    transform: rotate(45deg);
  }

  .chevron.left:before {
    left: 0.25em;
    transform: rotate(-135deg);
  }
`;

export const BackButton = (props: IButtonProps) => {
  const chevronClass = `chevron left`;
  return (
    <StyledBackButton onClick={props.onClick}>
      <span className={chevronClass}></span>
      &nbsp;Back
    </StyledBackButton>
  );
};
