import React from "react";
import styled from "styled-components";
import { BackButton, ChevronDirection } from "../Button/Button";
import { images } from "../../public";
import { NavImg } from "../ImgHeader/ImgHeader";

const Title = styled.p`
  text-shadow: 2px 2px 0 rgba(0, 0, 0, 0.1);
  margin: 0;
  background: ${props => props.theme.black};
  font-size: 1.6rem;
  flex: 0.9;
  text-align: center;
  color: white;
  align-self: "center";
`;

const StyledHeader = styled.nav`
  display: flex;
  flex: 1;
  background-color: ${props => props.theme.black};
  flex-direction: row;
  justify-content: "space-between";
`;

interface IHeaderProps {
  title: string;
}

export const Header = (props: IHeaderProps) => (
  <StyledHeader>
    <BackButton
      title={"Back"}
      onClick={() => console.log("back")}
      chevron={ChevronDirection.Left}
    />
    <Title>
      {/* <Link href="/"> */}
      <p>{props.title}</p>
      {/* </Link> */}
    </Title>
    <NavImg uri={images.logoSM} />
  </StyledHeader>
);
