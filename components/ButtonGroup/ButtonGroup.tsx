import React from "react";
import styled from "styled-components";
import { theme } from "../../public/";
import { Button, IButtonProps } from "../Button/Button";

const StyledButtonGroup = styled.div`
  display: flex;
  flex-direction: column;
  button {
    border: none;
    flex: 1;
    display: flex;
    justify-content: space-between;
    align-self: stretch;
    line-height: 2.5;
    font-size: 1.5rem;
    font-weight: bold;
    text-align: left;
    padding: 5px 25px 10px 25px;
    background-image: linear-gradient(${theme.offWhite}, ${theme.blueGrey});
    box-shadow: inset 2px 2px 3px rgba(255, 255, 255, 0.2),
      inset -2px -2px 3px rgba(0, 0, 0, 0.2);
    :first-child {
      border-top-left-radius: 10px;
      border-top-right-radius: 10px;
    }
    :last-child {
      border-bottom-left-radius: 10px;
      border-bottom-right-radius: 10px;
    }
    .chevron::before {
      border-style: solid;
      border-width: 0.15em 0.15em 0 0;
      content: "";
      color: ${theme.grey};
      display: inline-block;
      height: 0.6em;
      transform: rotate(-45deg);
      width: 0.6em;
    }

    .chevron.right:before {
      left: 0;
      right: 0;
      transform: rotate(45deg);
    }
  }
`;

interface IButtonGroupProps {
  buttons: Array<IButtonProps>;
}

export const ButtonGroup = (props: IButtonGroupProps) => (
  <StyledButtonGroup>
    {props.buttons.map(button => (
      <Button key={button.title} {...button} />
    ))}
  </StyledButtonGroup>
);
