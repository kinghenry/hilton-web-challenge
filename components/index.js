export * from "./Header/Header";
export * from "./Page";
export * from "./HotelDetails/HotelDetails";
export * from "./ButtonGroup/ButtonGroup";
export * from "./Button/Button";
export * from "./ImgHeader/ImgHeader";
