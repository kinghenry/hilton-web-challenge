import React from "react";
import styled from "styled-components";
import { theme } from "../../public";

const StyledContent = styled.div`
  flex-direction: column;
  .hotelText {
    margin: 5px;
  }
  p {
    font-size: 1rem;
    color: ${theme.white};
  }
  .addressText {
    color: ${theme.offWhite};
  }
  .phone {
    text-decoration-line: underline;
  }
  h3 {
    color: ${theme.white};
  }
`;

interface IStyledContentProps {
  hotelName: string;
  address1: string;
  address2: string;
  phone: string;
}

export const HotelDetails = (props: IStyledContentProps) => (
  <StyledContent>
    <div>
      <h3 className={"hotelText"}>{props.hotelName}</h3>
      <p className={"hotelText addressText"}>{props.address1}</p>
      <p className={"hotelText addressText"}>{props.address2}</p>
      <p className="hotelText phone">{props.phone}</p>
    </div>
  </StyledContent>
);
