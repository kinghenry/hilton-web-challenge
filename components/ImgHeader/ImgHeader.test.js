import { render, cleanup, screen } from "@testing-library/react";
import { ImgHeader } from "./ImgHeader";

afterEach(cleanup);

describe("Header test", () => {
  test("shows the Header with proper title", () => {
    it("renders", () => {
      const title = "Hotel Details";
      render(<Header title={title} />);
      expect(screen.queryByText(testMessage)).toBeNull();
    });
  });
});
