import React from "react";
import styled from "styled-components";

const StyledImg = styled.img`
  margin-top: 10px;
  margin-bottom: 10px;
  margin-left: auto;
  margin-right: auto;
  border: 5px solid white;
  text-align: center;
  max-width: 90%;
`;

export interface IImgProps {
  uri: string;
}

export const ImgHeader = (props: IImgProps) => <StyledImg src={props.uri} />;

const StyledNavImg = styled.img`
  margin-right: 10px;
  text-align: center;
  width: 50px;
  flex: 0.1;
`;

export const NavImg = (props: IImgProps) => <StyledNavImg src={props.uri} />;
