import React, { Component } from "react";
import styled, { ThemeProvider, createGlobalStyle } from "styled-components";
import { Header } from "../components";
import { images, theme } from "../public";

const StyledPage = styled.div`
  background: url(${images.background});
  color: ${props => props.theme.black};
`;

const Inner = styled.div`
  max-width: ${props => props.theme.maxWidth};
  /* background: ${props => props.theme.red}; */
  margin: 0 auto;
  padding: 2rem;
`;

createGlobalStyle`
  @font-face {
    font-family: 'radnika_next';
    src: url('../public/radnikanext-medium-webfont.woff2')
    format('woff2');
    font-weight: normal;
    font-style: normal;
  }
  html{
    padding: 0;
    margin: 0;
    box-sizing: border-box;
    font-size: 10px;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  body {
    padding: 0;
    margin: 0;
    font-size: 1.5rem;
    line-height: 2;
    font-family: 'radnika_next';
  }
  a {
    text-decoration: none;
    color: {theme.white};
  }
`;

export class Page extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <StyledPage>
          <Header title={"Hotel Details"} />
          <Inner>{this.props.children}</Inner>
        </StyledPage>
      </ThemeProvider>
    );
  }
}
