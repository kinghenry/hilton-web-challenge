module.exports = {
  rootDir: "",
  scriptPreprocessor: "<rootDir>/node_modules/babel-jest",
  testPathIgnorePatterns: ["<rootDir>/.next/", "<rootDir>/node_modules/"],
  transformIgnorePatterns: ["<rootDir>/.next/", "<rootDir>/node_modules/"]
};
